//
//  AppContainer.swift
//  RickAndMorty
//
//  Created by nourhan hamada on 08/12/2022.
//

import Foundation

let app = AppContainer()

final class AppContainer {
    let router = AppRouter()
}
