//
//  CharacterDetailRouter.swift
//  RickAndMorty
//
//  Created by nourhan hamada on 08/12/2022.
//

import Foundation

protocol CharacterDetailRoutingLogic: AnyObject {
    // nothing to handle
}

protocol CharacterDetailDataPassing: class {
    var dataStore: CharacterDetailDataStore? { get }
}

final class CharacterDetailRouter: CharacterDetailRoutingLogic, CharacterDetailDataPassing {
    
    weak var viewController: CharacterDetailViewController?
    var dataStore: CharacterDetailDataStore?
    
}
