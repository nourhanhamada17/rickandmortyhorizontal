//
//  CharacterListRouterSpy.swift
//  RickAndMortyTests
//
//  Created by nourhan hamada on 08/12/2022.
//

import Foundation
@testable import RickAndMorty

class CharacterListRouterSpy: CharacterListRoutingLogic, CharacterListDataPassing {
    var dataStore: CharacterListDataStore?
    
    var routeToCharacterDetailCalled = false
    func routeToCharacterDetail() {
        routeToCharacterDetailCalled = true
    }
}
